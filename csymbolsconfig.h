#ifndef CSYMBOLSCONFIG_H
#define CSYMBOLSCONFIG_H

/**

  Class containing a symbol configuradtion for a slot machine - describes values for symbols, and their ratios

*/

class CSymbolsConfig
{  
public:
  enum Symbols
  {
    J_symbol = 0,  //standard symbols (low win)
    Q_symbol,
    K_symbol,
    A_symbol,
    Snake,  // predator symbols
    Eagle,
    Bear,
    Lion,
    Scatter,
    Symbols_Count // always last entry
  };

  struct Wages
  {
    int three;
    int four;
    int five;
  };

  CSymbolsConfig();
  ~CSymbolsConfig(){}

  void SetValues(Wages _values[7]);
  void SetValue(Symbols _symbol, Wages _value);

  Wages values[8];
};

#endif // CSYMBOLSCONFIG_H
