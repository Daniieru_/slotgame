#include "cargs.h"


CArgs::CArgs(int _argc, char* _argv[])
{
  argc = _argc;
  argv.resize(argc);
  copy(_argv, _argv + argc, argv.begin());

  if(argc < 2)
  {
    is_valid = false;
    cout << "Invalid arguments\nAvailable arguments: " << arguments << "\n";
    return;
  }

  vector<string>::iterator it1, it2;
  it1 = argv.begin();
  it2 = it1 + 1;

  while(it1 != argv.end() && it2 != argv.end())
  {
    if((*it1)[0] == '-')
      arg_map[*it1] = *it2;

    it1++;
    it2++;
  }
  if(!Validate())
  {
    is_valid = false;
    cout << "Invalid arguments\nAvailable arguments: " << arguments << "\n";
  }
}

CArgs::~CArgs()
{

}
// checks if passed arguments are valid
bool CArgs::Validate()
{
  for(map<string, string>::iterator it = arg_map.begin(); it != arg_map.end(); it++)
  {
    if (arguments.find(it->first) == string::npos)
      return false;
  }
  return true;
}

int CArgs::Get_gamesCount()
{
  string ret = (Get_arg("-gamesCount") == "") ? "0" : Get_arg("-gamesCount");
  return stoi(ret);
}

int CArgs::Get_startCredit()
{
  string ret = (Get_arg("-startCredit") == "") ? "0" : Get_arg("-startCredit");
  return stoi(ret);
}

int CArgs::Get_gameBid()
{
  string ret = (Get_arg("-gameBid") == "") ? "0" : Get_arg("-gameBid");
  return stoi(ret);
}

string CArgs::Get_creditOutFile()
{
  return (Get_arg("-creditOutFile") == "") ? "" : Get_arg("-creditOutFile");
}

bool CArgs::Is_Valid()
{
  return is_valid;
}

string CArgs::Get_arg(int i)
{
  if( i < 0 || i > argc)
    return "";
  else
    return argv[i];
}

string CArgs::Get_arg(string _arg_name)
{
  if(arg_map.find(_arg_name) != arg_map.end())
    return arg_map[_arg_name];
  else
    return "";
}
