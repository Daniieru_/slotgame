#ifndef CARGS_H
#define CARGS_H

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <iostream>

using namespace std;

class CArgs
{
public:
  CArgs(int _argc, char* _argv[]);
  ~CArgs();

  string Get_arg(int i);
  string Get_arg(string _arg_name);
  bool Validate();
  int Get_gamesCount();
  int Get_startCredit();
  int Get_gameBid();
  string Get_creditOutFile();
  bool Is_Valid();


private:
  int argc;
  vector<string> argv;
  map<string, string> arg_map;
  bool is_valid;
  string arguments = "-gamesCount -startCredit -gameBid -creditOutFile";
};

#endif // CARGS_H
