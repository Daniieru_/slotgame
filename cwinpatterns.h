#ifndef CWINPATTERNS_H
#define CWINPATTERNS_H


class CWinPatterns
{
public:
  struct Pattern
  {
    int pos[5];
  };

  CWinPatterns();
  ~CWinPatterns(){}

  Pattern patterns[40];
  const int patterns_count = 40;
};

#endif // CWINPATTERNS_H
