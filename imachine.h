#ifndef IMACHINE_H
#define IMACHINE_H

#include<vector>
#include<memory>
#include "cwinpatterns.h"
#include "csymbolsconfig.h"

/**

  An interface for a 5-reels slot game machine

*/

class IMachine
{
public:
  IMachine();
  IMachine(double _balance, int _reel_size = 75);
  ~IMachine(){}

  virtual double GetBalance()=0;
  virtual void AddCash(double _amount)=0;
  virtual void Spin()=0;
  void SetUpReel(int _reel_no, const int *_ratios);
  void SetUpReels(const int *_ratios);
  void ShuffleReel(int _reel_no);
  void ShuffleReels();
  void AddScatters(int _reel_no, int _amount);
  void AddScatters();

protected:
  double balance;
  int reel_size;
  std::vector<int> reels[5];
  std::unique_ptr<CSymbolsConfig> symbol_config;
  std::unique_ptr<CWinPatterns> win_patterns;
};

#endif // IMACHINE_H
