#include "creport.h"
#include <iostream>
#include <iomanip>
#include <math.h>

CReport::CReport()
{

}

CReport::CReport(int _bets_count, int _bid)
{
  bets_count = _bets_count;
  bid = _bid;
  total_bids = bets_count * bid;

  total_win           = 0;
  total_free_spin_win = 0;
  win_count           = 0;
  top_win             = 0;

  for(int i = 0; i < 7; i++)
  {
    data_distribution[i].count = 0;
    data_distribution[i].value = 0;

    data_distribution_normal[i].count = 0;
    data_distribution_normal[i].value = 0;

    data_distribution_free_spin[i].count = 0;
    data_distribution_free_spin[i].value = 0;
  }
}


void CReport::AddToReport(int _value, int _scatters)
{
  if(_value > 0)
  {
    if(_value > top_win)
      top_win = _value;

    std::unique_ptr<Data> data_arr_ptr(new Data[7]);
    if(_scatters)
      data_arr_ptr.reset(data_distribution_free_spin);
    else
      data_arr_ptr.reset(data_distribution_normal);


    win_count++;

    ///add value to the right data distribution class
    if(_value < 11)
    {
      data_distribution[0].count++;
      data_distribution[0].value += _value;

      data_arr_ptr.get()[0].count++;
      data_arr_ptr.get()[0].value += _value;
    }
    else if(_value < 51)
    {
      data_distribution[1].count++;
      data_distribution[1].value += _value;

      data_arr_ptr.get()[1].count++;
      data_arr_ptr.get()[1].value += _value;
    }
    else if(_value < 101)
    {
      data_distribution[2].count++;
      data_distribution[2].value += _value;

      data_arr_ptr.get()[2].count++;
      data_arr_ptr.get()[2].value += _value;
    }
    else if(_value < 301)
    {
      data_distribution[3].count++;
      data_distribution[3].value += _value;

      data_arr_ptr.get()[3].count++;
      data_arr_ptr.get()[3].value += _value;
    }
    else if(_value < 601)
    {
      data_distribution[4].count++;
      data_distribution[4].value += _value;

      data_arr_ptr.get()[4].count++;
      data_arr_ptr.get()[4].value += _value;
    }
    else if(_value < 1001)
    {
      data_distribution[5].count++;
      data_distribution[5].value += _value;

      data_arr_ptr.get()[5].count++;
      data_arr_ptr.get()[5].value += _value;
    }
    else
    {
      data_distribution[6].count++;
      data_distribution[6].value += _value;

      data_arr_ptr.get()[6].count++;
      data_arr_ptr.get()[6].value += _value;
    }

    total_win += _value;

    data_arr_ptr.release(); ///we cant destroy our arrays
  }


  switch (_scatters)
  {
  case 3:
    free_spin_games[0]++;
    total_free_spin_win += _value;
    break;
  case 4:
    free_spin_games[1]++;
    total_free_spin_win += _value;
    break;
  case 5:
    free_spin_games[2]++;
    total_free_spin_win += _value;
    break;
  default:
    break;
  }

}

void CReport::PrintData(CReport::Data *data_arr)
{
  std::cout << "range   \t\tcount\t\tcount %\t\tvalue\t\tvalue %" << std::left << std::endl;

  for(int i = 0; i < 7; i++)
  {
    std::cout << range_str(i) << std::setw(9);
    std::cout << "\t"   << data_arr[i].count << std::left;
    std::cout << "\t\t"   << Percentage(data_arr[i].count, win_count) << std::left;
    std::cout << "\t\t"   << data_arr[i].value << std::left;
    std::cout << "\t\t"   << Percentage(data_arr[i].value, total_win) << std::left << std::endl;
  }
}

void CReport::Print()
{
  int free_spin_games_sum = free_spin_games[0] + free_spin_games[1] + free_spin_games[2];
  double rtp = Percentage(total_win, total_bids, 3);
  double hf  = Percentage(win_count, bets_count);


  std::cout << "******************** Raport Skrócony ********************" << std::endl;
  std::cout << "Liczba gier:\t" << bets_count << std::endl;
  std::cout << "Stawka gry:\t"  << bid << std::endl << std::endl;

  std::cout << "Rozkład wygranych całkowity" << std::endl;
  PrintData(data_distribution);
  std::cout << std::endl;

  std::cout << "Rozkład wygranych dla gier zwykłych" << std::endl;
  PrintData(data_distribution_normal);
  std::cout << std::endl;

  std::cout << "Rozkład wygranych dla gier w trybie free spin" << std::endl;
  PrintData(data_distribution_free_spin);
  std::cout << std::endl;

  std::cout << "Udział wygranych z sesji free spinowych : " << Percentage(total_free_spin_win, total_win) << "%" << std::endl;
  std::cout << "Najwyższa wygrana podczas gier : " << top_win << std::endl;
  std::cout << "Ilość sesji free spin (3x scatter) : " << free_spin_games[0] << std::endl;
  std::cout << "Ilość sesji free spin (4x scatter) : " << free_spin_games[1] << std::endl;
  std::cout << "Ilość sesji free spin (5x scatter) : " << free_spin_games[2] << std::endl;
  std::cout << "Wystąpienia sesji free spin : " << free_spin_games_sum << "(średnio raz na " << bets_count / free_spin_games_sum << " razy)" << std::endl;
  std::cout << "Całkowity RTP: " << rtp << "%, całkowity HF: " << hf << "%" << std::endl;
  std::cout << "suma stawek: " << total_bids << ", suma wygranych: " << total_win << std::endl;
  std::cout << "wygrane gry (sesje free spinowe liczone jako jedna gra): " << win_count << std::endl << std::endl;


}

void CReport::CreateOutputFile(std::string _file_name)
{
  output_file.open(_file_name);
  output_line = 0;
  if( output_file.is_open() )
  {
    output_file << "Lp.\t" << std::setw(20) << "Stan\t" << std::setw(20) << "Zmiana" << std::endl << std::endl;
  }
}

void CReport::Log(int _balance, int _difference)
{
  if( !output_file.is_open() )
  {
    std::cout << "Output_file is not open." << std::endl;
    return;
  }

  output_file<< std::setw(8) << ++output_line << ".\t"<< std::setw(15) << _balance << "\t"<< std::setw(15)  << _difference << std::endl;
}

std::string CReport::range_str(int i)
{
  switch (i) {
  case 0:
    return "1-10    ";
    break;
  case 1:
    return "11-50   ";
    break;
  case 2:
    return "51-100  ";
    break;
  case 3:
    return "101-300 ";
    break;
  case 4:
    return "301-600 ";
    break;
  case 5:
    return "601-1000";
    break;
  case 6:
    return "1001-...";
    break;
  default:
    return "";
    break;
  }
}

// return a percentage value rounded to given decimal places
double CReport::Percentage(int _partial_value, int _full_value, int _places)
{
  double tmp = std::pow(10.0, (double)_places);
  return std::round( (_partial_value * 100.0) / _full_value * tmp ) /  tmp;
}
