#include "imachine.h"
#include <time.h>
#include <algorithm>
#include <random>
#include <chrono>

IMachine::IMachine()
{
}

IMachine::IMachine(double _balance, int _reel_size)
{
  this->balance = _balance;
  this->reel_size = _reel_size;
}

// set random symbols to the reel according to the ratios
void IMachine::SetUpReel(int _reel_no, const int *_ratios)
{
  reels[_reel_no].clear();

  for(int i = 0; i < CSymbolsConfig::Symbols_Count - 1; i++)
  {

    int count = (int)( reel_size * (double)(_ratios[i] / 100.0) );
    for(int j = 0; j < count; j++)
    {
      reels[_reel_no].push_back(i);
    }
  }

  ShuffleReel(_reel_no);
}

void IMachine::SetUpReels(const int *_ratios)
{
  for(int i = 0; i < 5; i++)
    SetUpReel(i, _ratios);
}

void IMachine::ShuffleReel(int _reel_no)
{
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::default_random_engine rng( seed );
  std::shuffle( reels[_reel_no].begin(), reels[_reel_no].end(), rng );
}

void IMachine::ShuffleReels()
{
  for(int i = 0; i < 5; i++)
    ShuffleReel(i);
}

void IMachine::AddScatters(int _reel_no, int _amount)
{
  for(int i = 0; i < _amount; i++)
    this->reels[_reel_no].push_back( CSymbolsConfig::Scatter );
}

void IMachine::AddScatters()
{
  int scatters_to_add = (int)( reel_size * (double)(5 / 100.0) );
  for(int i = 0; i < 5; i++)
    AddScatters( i, scatters_to_add );

  ShuffleReels();
}
