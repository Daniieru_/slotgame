#include <iostream>
#include <algorithm>
#include <string>
#include "cmachine.h"
#include "cargs.h"

int main(int argc, char* argv[])
{
  CArgs args(argc, argv);

  if(!args.Is_Valid())
    return 1;

  int startCredit = args.Get_startCredit();
  int gamesCount  = args.Get_gamesCount();
  int gameBid     = args.Get_gameBid();
  string out_file = args.Get_creditOutFile();

  if(gameBid == 0)
    gameBid = 20; /// 20 is default game bid value


  std::unique_ptr<CMachine> machine( new CMachine(startCredit, 75) );

  if(out_file == "")
    machine->Play(gameBid, gamesCount);
  else
    machine->Play(gameBid, gamesCount, out_file);

  return 0;
}
