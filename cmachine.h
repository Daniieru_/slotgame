#ifndef CMACHINE_H
#define CMACHINE_H

#include "imachine.h"

class CMachine : public IMachine
{
public:

  CMachine();
  CMachine(double _balance, int _reel_size);
  ~CMachine();

  double GetBalance();
  void AddCash(double _amount);
  void Spin();
  int FreeSpins(int _bid, int _scatter_count);
  void Play(int _bid, int _count, std::string _outFile = "credit_log.txt");
  int CheckWin(int _bet = 20);
  int CheckScatters();

  void Draw();

private:
  int current_set[5][4];
  int normal_ratios[8];//  = {15, 10, 10, 10, 10, 10, 25, 10};
  int normal_ratios2[8];//  = {15, 10, 10, 10, 10, 10, 25, 10};
  int freespin_ratios[8];// = {15, 10, 10, 10, 10, 10, 25, 10};

  int GetWinPatternValue(int _pattern_no);
  void PrintReport();
  void CustomReelsSetUp();
};

#endif // CMACHINE_H
