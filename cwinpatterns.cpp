#include "cwinpatterns.h"

CWinPatterns::CWinPatterns()
{
  patterns[0].pos[0] = 2;
  patterns[0].pos[1] = 2;
  patterns[0].pos[2] = 2;
  patterns[0].pos[3] = 2;
  patterns[0].pos[4] = 2;

  patterns[1].pos[0] = 1;
  patterns[1].pos[1] = 1;
  patterns[1].pos[2] = 1;
  patterns[1].pos[3] = 1;
  patterns[1].pos[4] = 1;

  patterns[2].pos[0] = 3;
  patterns[2].pos[1] = 3;
  patterns[2].pos[2] = 3;
  patterns[2].pos[3] = 3;
  patterns[2].pos[4] = 3;

  patterns[3].pos[0] = 0;
  patterns[3].pos[1] = 0;
  patterns[3].pos[2] = 0;
  patterns[3].pos[3] = 0;
  patterns[3].pos[4] = 0;

  patterns[4].pos[0] = 0;
  patterns[4].pos[1] = 1;
  patterns[4].pos[2] = 2;
  patterns[4].pos[3] = 1;
  patterns[4].pos[4] = 0;

  patterns[5].pos[0] = 1;
  patterns[5].pos[1] = 2;
  patterns[5].pos[2] = 3;
  patterns[5].pos[3] = 2;
  patterns[5].pos[4] = 1;

  patterns[6].pos[0] = 3;
  patterns[6].pos[1] = 2;
  patterns[6].pos[2] = 1;
  patterns[6].pos[3] = 2;
  patterns[6].pos[4] = 3;

  patterns[7].pos[0] = 2;
  patterns[7].pos[1] = 1;
  patterns[7].pos[2] = 0;
  patterns[7].pos[3] = 1;
  patterns[7].pos[4] = 2;

  patterns[8].pos[0] = 0;
  patterns[8].pos[1] = 1;
  patterns[8].pos[2] = 0;
  patterns[8].pos[3] = 1;
  patterns[8].pos[4] = 0;

  patterns[9].pos[0] = 1;
  patterns[9].pos[1] = 2;
  patterns[9].pos[2] = 1;
  patterns[9].pos[3] = 2;
  patterns[9].pos[4] = 1;

  patterns[10].pos[0] = 2;
  patterns[10].pos[1] = 3;
  patterns[10].pos[2] = 2;
  patterns[10].pos[3] = 3;
  patterns[10].pos[4] = 2;

  patterns[11].pos[0] = 3;
  patterns[11].pos[1] = 2;
  patterns[11].pos[2] = 3;
  patterns[11].pos[3] = 2;
  patterns[11].pos[4] = 3;

  patterns[12].pos[0] = 2;
  patterns[12].pos[1] = 1;
  patterns[12].pos[2] = 2;
  patterns[12].pos[3] = 1;
  patterns[12].pos[4] = 2;

  patterns[13].pos[0] = 1;
  patterns[13].pos[1] = 0;
  patterns[13].pos[2] = 1;
  patterns[13].pos[3] = 0;
  patterns[13].pos[4] = 1;

  patterns[14].pos[0] = 2;
  patterns[14].pos[1] = 2;
  patterns[14].pos[2] = 3;
  patterns[14].pos[3] = 2;
  patterns[14].pos[4] = 2;

  patterns[15].pos[0] = 1;
  patterns[15].pos[1] = 1;
  patterns[15].pos[2] = 2;
  patterns[15].pos[3] = 1;
  patterns[15].pos[4] = 1;

  patterns[16].pos[0] = 0;
  patterns[16].pos[1] = 0;
  patterns[16].pos[2] = 1;
  patterns[16].pos[3] = 0;
  patterns[16].pos[4] = 0;

  patterns[17].pos[0] = 1;
  patterns[17].pos[1] = 1;
  patterns[17].pos[2] = 0;
  patterns[17].pos[3] = 1;
  patterns[17].pos[4] = 1;

  patterns[18].pos[0] = 2;
  patterns[18].pos[1] = 2;
  patterns[18].pos[2] = 1;
  patterns[18].pos[3] = 2;
  patterns[18].pos[4] = 2;

  patterns[19].pos[0] = 3;
  patterns[19].pos[1] = 3;
  patterns[19].pos[2] = 2;
  patterns[19].pos[3] = 3;
  patterns[19].pos[4] = 3;

  patterns[20].pos[0] = 0;
  patterns[20].pos[1] = 0;
  patterns[20].pos[2] = 3;
  patterns[20].pos[3] = 0;
  patterns[20].pos[4] = 0;

  patterns[21].pos[0] = 0;
  patterns[21].pos[1] = 0;
  patterns[21].pos[2] = 2;
  patterns[21].pos[3] = 0;
  patterns[21].pos[4] = 0;

  patterns[22].pos[0] = 1;
  patterns[22].pos[1] = 1;
  patterns[22].pos[2] = 3;
  patterns[22].pos[3] = 1;
  patterns[22].pos[4] = 1;

  patterns[23].pos[0] = 3;
  patterns[23].pos[1] = 3;
  patterns[23].pos[2] = 0;
  patterns[23].pos[3] = 3;
  patterns[23].pos[4] = 3;

  patterns[24].pos[0] = 3;
  patterns[24].pos[1] = 3;
  patterns[24].pos[2] = 1;
  patterns[24].pos[3] = 3;
  patterns[24].pos[4] = 3;

  patterns[25].pos[0] = 2;
  patterns[25].pos[1] = 2;
  patterns[25].pos[2] = 0;
  patterns[25].pos[3] = 2;
  patterns[25].pos[4] = 2;

  patterns[26].pos[0] = 0;
  patterns[26].pos[1] = 2;
  patterns[26].pos[2] = 0;
  patterns[26].pos[3] = 2;
  patterns[26].pos[4] = 0;

  patterns[27].pos[0] = 1;
  patterns[27].pos[1] = 3;
  patterns[27].pos[2] = 1;
  patterns[27].pos[3] = 3;
  patterns[27].pos[4] = 1;

  patterns[28].pos[0] = 3;
  patterns[28].pos[1] = 1;
  patterns[28].pos[2] = 3;
  patterns[28].pos[3] = 1;
  patterns[28].pos[4] = 3;

  patterns[29].pos[0] = 2;
  patterns[29].pos[1] = 0;
  patterns[29].pos[2] = 2;
  patterns[29].pos[3] = 0;
  patterns[29].pos[4] = 2;

  patterns[30].pos[0] = 3;
  patterns[30].pos[1] = 2;
  patterns[30].pos[2] = 2;
  patterns[30].pos[3] = 2;
  patterns[30].pos[4] = 3;

  patterns[31].pos[0] = 2;
  patterns[31].pos[1] = 1;
  patterns[31].pos[2] = 1;
  patterns[31].pos[3] = 1;
  patterns[31].pos[4] = 2;

  patterns[32].pos[0] = 1;
  patterns[32].pos[1] = 0;
  patterns[32].pos[2] = 0;
  patterns[32].pos[3] = 0;
  patterns[32].pos[4] = 1;

  patterns[33].pos[0] = 0;
  patterns[33].pos[1] = 1;
  patterns[33].pos[2] = 1;
  patterns[33].pos[3] = 1;
  patterns[33].pos[4] = 0;

  patterns[34].pos[0] = 1;
  patterns[34].pos[1] = 2;
  patterns[34].pos[2] = 2;
  patterns[34].pos[3] = 2;
  patterns[34].pos[4] = 1;

  patterns[35].pos[0] = 2;
  patterns[35].pos[1] = 3;
  patterns[35].pos[2] = 3;
  patterns[35].pos[3] = 3;
  patterns[35].pos[4] = 2;

  patterns[36].pos[0] = 3;
  patterns[36].pos[1] = 1;
  patterns[36].pos[2] = 1;
  patterns[36].pos[3] = 1;
  patterns[36].pos[4] = 3;

  patterns[37].pos[0] = 2;
  patterns[37].pos[1] = 0;
  patterns[37].pos[2] = 0;
  patterns[37].pos[3] = 0;
  patterns[37].pos[4] = 2;

  patterns[38].pos[0] = 1;
  patterns[38].pos[1] = 3;
  patterns[38].pos[2] = 3;
  patterns[38].pos[3] = 3;
  patterns[38].pos[4] = 1;

  patterns[39].pos[0] = 0;
  patterns[39].pos[1] = 2;
  patterns[39].pos[2] = 2;
  patterns[39].pos[3] = 2;
  patterns[39].pos[4] = 0;

}
