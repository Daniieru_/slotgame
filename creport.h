#ifndef CREPORT_H
#define CREPORT_H

#include <string>
#include <memory>
#include <fstream>

class CReport
{
public:
  struct Data
  {
    int count;
    int value;
  };

  CReport();
  CReport(int _bets_count, int _bid);
  ~CReport(){ output_file.close(); }

  void AddToReport(int _value, int _scatters);
  void PrintData(Data *data_arr);
  void Print();
  void CreateOutputFile(std::string _file_name);
  void Log(int _balance, int _difference);

private:
  int bid;
  int bets_count;
  int total_bids;
  int total_win;
  int total_free_spin_win;
  int top_win;
  int win_count;
  int free_spin_games[3];             ///[0] - 3x scatters sessions, [1] - 4x ..., [2] - 5x ...,

  Data data_distribution[7];          ///[0] - wins in range 1-10, [1] - in range 11-50... (7 distribution classes)
  Data data_distribution_normal[7];   ///[0] - wins in range 1-10, [1] - in range 11-50...
  Data data_distribution_free_spin[7];///[0] - wins in range 1-10, [1] - in range 11-50...

  int output_line;
  std::ofstream output_file;

  std::string range_str(int i);       ///returns a string with range for a given [i] in distribution array
  double Percentage(int _partial_value, int _full_value, int _places = 2);
};

#endif // CREPORT_H
