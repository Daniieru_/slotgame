#include "cmachine.h"
#include <random>
#include <iostream>
#include <string>
#include <chrono>

#include "creport.h"

CMachine::CMachine()
{

}

CMachine::CMachine(double _balance, int _reel_size) : IMachine(_balance, _reel_size)
{
  symbol_config.reset( new CSymbolsConfig() );
  win_patterns.reset( new CWinPatterns() );

  int normal_ratios_init_arr[8]   = {26, 15, 8, 8, 11, 10, 9, 13};
  int normal_ratios2_init_arr[8]  = {2, 4, 5, 5, 5, 5, 29, 45};
  int freespin_ratios_init_arr[8] = {5, 5, 5, 11, 10, 13, 12, 39};

  std::copy(std::begin(normal_ratios_init_arr), std::end(normal_ratios_init_arr), std::begin(normal_ratios));
  std::copy(std::begin(normal_ratios2_init_arr), std::end(normal_ratios2_init_arr), std::begin(normal_ratios2));
  std::copy(std::begin(freespin_ratios_init_arr), std::end(freespin_ratios_init_arr), std::begin(freespin_ratios));

  CustomReelsSetUp();

}

CMachine::~CMachine()
{
}

double CMachine::GetBalance()
{
  return this->balance;
}

void CMachine::AddCash(double _amount)
{
  this->balance += _amount;
}

//spin all reels - changes the current symbol set.
void CMachine::Spin()
{
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::minstd_rand0 rng(seed);

  for(int i = 0; i < 5; i++)
  {
    int pos = rng() % this->reels[i].size();

    for(int j = 0; j < 4; j++)
    {
      if(pos < 0)
        pos = this->reels[i].size() - 1;

      this->current_set[i][j] = reels[i].at(pos--);
    }
  }

}
// with given bid value and scatter count on current reels set simulate a free spin session and return its prize
int CMachine::FreeSpins(int _bid, int _scatter_count)
{
  SetUpReels(freespin_ratios);

  int prize_ = 0;

  switch (_scatter_count) {
  case 3:
    for(int i = 0; i < 10; i++)
    {
      Spin();
      prize_ += CheckWin(_bid);
    }
    break;
  case 4:
    for(int i = 0; i < 15; i++)
    {
      Spin();
      prize_ += CheckWin(_bid);
    }
    break;
  case 5:
    for(int i = 0; i < 20; i++)
    {
      Spin();
      prize_ += CheckWin(_bid);
    }
    break;
  default:
    break;
  }

  CustomReelsSetUp();

  return prize_;
}

// simulates a given amount of games with a given single bid value and generates a report
// default output file name: 'credit_log.txt'
void CMachine::Play(int _bid, int _count, std::string _outFile)
{
  int free_spin_win = 0;
  int scatters = 0;
  int spin_win = 0;

  std::unique_ptr<CReport> report( new CReport(_count, _bid) );
  report->CreateOutputFile(_outFile);

  for(int i = 0; i < _count; i++)
  {
    spin_win = 0;
    free_spin_win = 0;

    int x_balance = balance;
    balance -= _bid;

    Spin();
    scatters = CheckScatters();

    spin_win = CheckWin();
    free_spin_win = FreeSpins(_bid, scatters);

    report->AddToReport(spin_win, 0);
    report->AddToReport(free_spin_win, scatters);

    spin_win += free_spin_win;


    if(spin_win)
    {
      balance += spin_win;
    }

    report->Log(balance, balance - x_balance);

  }

  report->Print();

}

// checks and returns a game prize
int CMachine::CheckWin(int _bid)
{
  int ret_val_ = 0;
  double multipier = (double)_bid / 20.0;
  for(int i = 0; i < win_patterns->patterns_count; i++)
  {
    ret_val_ += GetWinPatternValue(i);
  }
  return ret_val_ * multipier;
}

// counts scatter symbols
int CMachine::CheckScatters()
{
  int scatter_count_ = 0;

  for(int i = 0; i < 5; i++)
    for(int j = 0; j < 4; j++)
      if(current_set[i][j] == CSymbolsConfig::Scatter)
        scatter_count_++;

  return scatter_count_;
}

void CMachine::Draw()
{
  for(int i = 3; i > -1; i--)
  {
    for(int j = 0; j < 5; j++)
    {
      std::cout << this->current_set[j][i];
      std::cout << "\t";
    }
    std::cout << std::endl;
  }
}

// returns a win value for certain pattern
int CMachine::GetWinPatternValue(int _pattern_no)
{
  int pattern_set[5];
  int symbols_match = 1;

  for(int i = 0; i < 5; i++)
  {
    pattern_set[i] = current_set[i][win_patterns->patterns[_pattern_no].pos[i]];
  }

  for(int i = 1; i < 5; i++)
  {
    if(pattern_set[0] == pattern_set[i] && pattern_set[0] != CSymbolsConfig::Scatter)
      symbols_match++;
    else
      break;
  }

  switch(symbols_match)
  {
  case 3:
    return symbol_config->values[pattern_set[0]].three;

  case 4:
    return symbol_config->values[pattern_set[0]].four;

  case 5:
    return symbol_config->values[pattern_set[0]].five;

  default:
    return 0;
  }
}

void CMachine::CustomReelsSetUp()
{
  SetUpReel(0, normal_ratios);
  SetUpReel(1, normal_ratios);
  SetUpReel(2, normal_ratios2);
  SetUpReel(3, normal_ratios2);
  SetUpReel(4, normal_ratios2);

  AddScatters(0, 2);
  AddScatters(1, 1);
  AddScatters(2, 1);
  AddScatters(3, 1);
  AddScatters(4, 1);
}

