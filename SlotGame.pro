TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    imachine.cpp \
    csymbolsconfig.cpp \
    cwinpatterns.cpp \
    cmachine.cpp \
    creport.cpp \
    cargs.cpp

HEADERS += \
    imachine.h \
    csymbolsconfig.h \
    cwinpatterns.h \
    cmachine.h \
    cmachine.h \
    creport.h \
    cargs.h
