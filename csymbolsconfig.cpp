#include "csymbolsconfig.h"

CSymbolsConfig::CSymbolsConfig()
{
  SetValue(J_symbol, {4, 8, 20});
  SetValue(Q_symbol, {4, 8, 20});
  SetValue(K_symbol, {5, 10, 25});
  SetValue(A_symbol, {5, 10, 25});

  SetValue(Snake,    {8, 15, 40});
  SetValue(Eagle,    {8, 15, 50});
  SetValue(Bear,     {10, 20, 75});
  SetValue(Lion,     {10, 25, 100});
}

void CSymbolsConfig::SetValues(Wages _values[])
{
  for(int i = 0; i < 7; i++)
  {
    this->values[i] = _values[i];
  }
}

void CSymbolsConfig::SetValue(CSymbolsConfig::Symbols _symbol, Wages _value)
{
  this->values[_symbol] = _value;
}

